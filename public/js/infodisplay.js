var setIntervals = [];
var setIntervalBackup = window.setInterval;
window.setInterval = function (fun, delay) {
    setIntervals.push(setIntervalBackup(fun, delay));
}

$(function () {
    var socket = io.connect();
    socket.on('meta', function (cmd) {
        if (cmd === 'reload') {
            document.location.reload();
        }
    });
    socket.on('connect', function () {
        var ident = sessionStorage.getItem('ident');
        if (!ident) {
            ident = Math.random().toString().substr(2);
            sessionStorage.setItem('ident', ident);
        }
        socket.emit('ident', ident);
    });
    socket.on('disconnect', function () {
        setIntervals.forEach(function (interval) {
            window.clearInterval(interval);
        });
        setIntervals = [];
    });
    $('.component').each(function () {
        var e = $(this);
        var outlets = new Set();
        socket.on(e.attr('id'), function (cnt) {
            e.html(cnt);
            e.find('[data-infodisplay-outlet]').each(function () {
                var name = $(this).attr('data-infodisplay-outlet');
                if (!outlets.has(name)) {
                    socket.on(e.attr('id')+'.'+name, function (content) {
                        $('[data-infodisplay-outlet="'+name+'"]').html(content);
                        e.trigger('content', name);
                    });
                    outlets.add(name);
                }
            });
        });
    });
});
