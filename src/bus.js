/* eslint no-mixed-operators: 0 */
import fs from 'fs';
import httpreq from 'httpreq';
import Mustache from 'mustache';
import XML from 'pixl-xml';

/// CONFIG
const CITY = 'Braunschweig';
const STOPS = ['Ludwigstraße', 'Hamburger Straße'];
const ENTRIES = 6;

/// VARS
let CACHED = [];
const TEMPLATES = {
  outer: fs.readFileSync('modules/bus/outer.mustache', 'utf-8'),
  inner: fs.readFileSync('modules/bus/inner.mustache', 'utf-8'),
};

function fetchData(stop, cb) {
  const url = `http://62.154.206.87/efaws2/default/XML_DM_REQUEST?sessionID=0&requestID=0&language=de&useRealtime=1&coordOutputFormat=WGS84[DD.ddddd]&locationServerActive=1&mode=direct&dmLineSelectionAll=1&depType=STOPEVENTS&useAllStops=1&command=null&type_dm=stop&name_dm=${ CITY } ${ stop }&mId=efa_rc2`;
  httpreq.get(url, { binary: true }, (err, res) => {
    try {
      cb(XML.parse(res.body));
    } catch (e) {
      // Swallow
    }
  });
}

function renderLine(line) {
  let image = '';
  if (line.charAt(0) === 'M') {
    image = 'metro.svg';
    line = line.substr(1); // eslint-disable-line
  } else if (line.length < 3) {
    image = 'tram.svg';
  } else {
    image = 'bus.svg';
  }
  return `<img src="/modules/bus/${image}"> ${line}`;
}

function getData(stop, count, cb) {
  fetchData(stop, (data) => {
    const deps = data.itdDepartureMonitorRequest.itdDepartureList.itdDeparture;
    cb(deps.slice(0, count).map((dep) => ({
      line: dep.itdServingLine.symbol,
      renderedLine: renderLine(dep.itdServingLine.symbol),
      dir: dep.itdServingLine.direction.replace(CITY, '').trim(),
      platform: dep.platform,
      mean: dep.itdServingLine.itdNoTrain.name,
      day: pad(dep.itdDateTime.itdDate.day, 2),
      month: pad(dep.itdDateTime.itdDate.month, 2),
      hour: pad(dep.itdDateTime.itdTime.hour, 2),
      minute: pad(dep.itdDateTime.itdTime.minute, 2),
    })));
  });
}

function update(io, allStopsDoneCb) {
  const done = [];
  const context = [];
  const innerGetData = function(stop, i) {
    const ns = normalizeStop(stop);
    getData(stop, ENTRIES, (deps) => {
      try {
        context[i] = { stop, normalizedStop: ns, deps };
        io.emit(`bus.${ns}`, Mustache.render(TEMPLATES.inner, context[i]));
        if (done.indexOf(stop) === -1) {
          done.push(stop);
          if (done.length === STOPS.length) {
            allStopsDoneCb();
          }
        }
        CACHED = context;
      } catch (e) {
        console.log(e); // eslint-disable-line
      }
      // calculate when to update next
      const d = new Date();
      const hour = d.getHours();
      const minute = d.getMinutes();
      const depHour = deps[0].hour < hour ? deps[0].hour + 24 : deps[0].hour;
      const diff = (depHour - hour) * 60 + (deps[0].minute - minute);
      setTimeout(() => {
        innerGetData(stop, i);
      }, (diff * 60 + (60 - d.getSeconds()) % 60) * 1000);
    });
  };
  STOPS.forEach((stop, i) => {
    innerGetData(stop, i);
  });
}

function normalizeStop(stop) {
  return stop.replace(/[^a-zA-Z0-9_]/g, '');
}

module.exports = function(io) {
  update(io, () => {
    const pushToClient = function(sock) {
      sock.emit('bus', Mustache.render(TEMPLATES.outer, CACHED));
      setTimeout(() => {
        for (const i in CACHED) { // eslint-disable-line
          sock.emit(`bus.${CACHED[i].normalizedStop}`,
            Mustache.render(TEMPLATES.inner, CACHED[i]));
          }
        }, 3000); //wait a second to let the client process the outlets
      };
      io.on('connect', pushToClient);
      pushToClient(io);
    });
  };
